import os, sys

import setuptools
from setuptools.command.install import install

module_path = os.path.join(os.path.dirname(__file__), 'FwValues/__init__.py')
version_line = [line for line in open(module_path)
                if line.startswith('__version__')][0]

__version__ = version_line.split('__version__ = ')[-1][1:-1]

            
class custom_install(install):
    def run(self):
        print("This is a custom installation")
        install.run(self)

setuptools.setup(
    name="FwValues",
    version=__version__,
    url="https://bitbucket.org/FindWatt/fwvalues",

    author="FindWatt",

    description="Value/Metadata utilities",
    long_description=open('README.md').read(),

    packages = setuptools.find_packages(),
    package_data={'': ["*.pyx","*.txt"]},
    py_modules=['FwValues'],
    zip_safe=False,
    platforms='any',

    install_requires=[
        "cython",
        "numpy",
        "scikit-learn",
        "scipy",
    ],
    cmdclass={'install': custom_install}
)


