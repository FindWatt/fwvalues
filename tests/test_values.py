﻿import pytest
import math, re
import numpy as np
import FwValues

class TestListValues():
    def test_is_empty(self):
        assert FwValues.is_empty(np.nan) == True
        assert FwValues.is_empty("") == True
        assert FwValues.is_empty(" ") == True
        assert FwValues.is_empty("0") == False
        assert FwValues.is_empty(0) == False
        assert FwValues.is_empty(0.0) == False
    
    def test_num_to_string(self):
        assert FwValues.num_to_string(np.nan) == ''
        assert FwValues.num_to_string(0) == '0'
        assert FwValues.num_to_string(0.0) == '0'
        assert FwValues.num_to_string(1) == '1'
        assert FwValues.num_to_string(1.1) == '1.1'
    
    def test_list_values(self):
        a_test = [None, np.nan, 0, 0.0, 1, 1.1, '', ' ', 'a']
        assert FwValues.list_values(a_test) == [0, 0.0, 1, 1.1, 'a']
    
    def test_to_float(self):
        assert FwValues.to_float('') == 0.0
        assert FwValues.to_float('0') == 0
        assert FwValues.to_float('1') == 1.0
        assert FwValues.to_float('1/2') == 0.5
        assert FwValues.to_float('1-2') == 1.0
        assert FwValues.to_float('-1/2') == -0.5
        assert FwValues.to_float('1-1/2') == 1.5
        assert FwValues.to_float('-1-1/2') == -1.5
        assert FwValues.to_float('1 1/2') == 1.5
        assert FwValues.to_float('-1 1/2') == -1.5
        assert FwValues.to_float('1.0') == 1.0
        assert FwValues.to_float('1.') == 1.0
        assert FwValues.to_float('1') == 1.0
        assert FwValues.to_float('911') == 911.0
        assert FwValues.to_float('0.1') == 0.1
        assert FwValues.to_float('.1') == 0.1
        assert FwValues.to_float('.10') == 0.1
        assert FwValues.to_float('100.0 %') == 1.0
        assert FwValues.to_float('1.0 %') == 0.01
        assert FwValues.to_float('.10 %') == 0.001
        assert FwValues.to_float('1234567.89') == 1234567.89
        assert FwValues.to_float('1234,567.89') == 1234567.89
        assert FwValues.to_float('1,234567.89') == 1234567.89
        assert FwValues.to_float('1,234567,.89') == 1234567.89
        assert np.isnan(FwValues.to_float([]))
        
    def test_list_numbers(self):
        a_test = [None, np.nan, 0, 0.0, 1, 1.1, '', ' ', 'a']
        assert FwValues.list_values(a_test) == [0, 0.0, 1, 1.1, 'a']
    
    def test_value_list_sum(self):
        a_test = [None, np.nan, 0, 0.0, 1, 2.5, '', ' ', 'a']
        assert FwValues.value_list_sum(a_test) == 3.5
        
    def test_value_list_avg(self):
        a_test = [None, np.nan, 0, 0.0, 1, 2.5, '', ' ', 'a']
        assert FwValues.value_list_avg(a_test) == 0.875
        
    def test_value_list_stddev(self):
        a_test = [None, np.nan, 0, 0.0, 1, 2.5, '', ' ', 'a']
        assert math.isclose(FwValues.value_list_stddev(a_test), 1.0231690964840563)
        
    def test_value_list_herfindahl(self):
        assert math.isclose(FwValues.value_list_herfindahl([]), 0)
        assert math.isclose(FwValues.value_list_herfindahl([100]), 1.0)
        assert math.isclose(FwValues.value_list_herfindahl([1, 3]), 0.625)
        assert math.isclose(FwValues.value_list_herfindahl([2, 2]), 0.5)
        
    def test_value_list_similarity(self):
        a_test = [None, np.nan, 0, 0.0, 1, 2.5, '', ' ', 'a']
        assert np.isnan(FwValues.value_list_similarity([]))
        assert math.isclose(FwValues.value_list_similarity([0,1,2.5]), 0.11936942814728912)
        assert math.isclose(FwValues.value_list_similarity([1,2.5]), .5714285714285714)
        assert math.isclose(FwValues.value_list_similarity([1,1]), 1.0)
        assert math.isclose(FwValues.value_list_similarity([0,1]), 0.0)
        assert math.isclose(FwValues.value_list_similarity([-1,1]), 0.0)
        
    def test_value_list_variation(self):
        assert np.isnan(FwValues.value_list_variation([]))
        assert math.isclose(FwValues.value_list_variation([0,1,2.5]), 1.0)
        assert math.isclose(FwValues.value_list_variation([1,2.5]), 0.6)
        assert math.isclose(FwValues.value_list_variation([-1,2.5]), 1.4)
        assert math.isclose(FwValues.value_list_variation([-1,-2.5]), 0.6)
        assert math.isclose(FwValues.value_list_variation([-1,0]), 1.0)


class TestNumberSet():
    def test_intialize_empty(self):
        cn_test = FwValues.NumberSet()
        print(cn_test)
        assert len(cn_test) == 0
    
    def test_intialize_values(self):
        cn_test = FwValues.NumberSet(['7.5', '7 - 1/2', '15/2', '7.500', '7.501', 7.5, "seven and a half"])
        print(cn_test)
        assert len(cn_test) == 1
    
    def test_add_numbers(self):
        cn_test = FwValues.NumberSet()
        for val in ['7.5', '7 - 1/2', '15/2', '7.500', '7.501', 7.5, "seven and a half"]:
            cn_test.add(val)
        print(cn_test)
        assert len(cn_test) == 1
    
    def test_no_tolerance(self):
        cn_test = FwValues.NumberSet(relative_tolerance=0)
        for val in ['7.5', '7 - 1/2', '15/2', '7.500', '7.501', 7.5, "seven and a half"]:
            cn_test.add(val)
        print(cn_test)
        assert len(cn_test) == 2


class TestNumberStandardizationCleaning():
    tests = [
        (" one_sixteenth-inch", " 0.0625-inch"),
        ("one_sixteenth inch", "0.0625 inch"),
        ("two + two = 4", "2 + 2 = 4"),
        ("one hundred minus two thousandths", "99.998"),
        ("a thousandth", "0.001"),
        ("two thousandths", "0.002"),
        ("one twenty-fourth", "0.0416667"),
        ("this has a no real number", "this has a no real number"),
        ("one thousand, nine hundred, ninety-nine", "1999"),
        ("one hundred dot six", "100.6"),
        ("three thousand point zero two", "3000.02"),
        ("one point zero zero zero two", "1.0002"),
        ("one two three test", "1 2 3 test"),
        ("one thousand two test", "1002 test"),
        ("twenty-four seven", "24/7"),
        ("three sixty five", "365"),
        ("nine one one", "911"),
        ("nine eleven", "9/11"),
        ("fifty-five and a half", "55.5"),
        ("ten halves", "5"),
        ("2 dozen eggs", "24 eggs"),
        ("one gross straws", "144 straws"),
        ("", ""),
    ]
        
    def test_clean_number_words(self):
        i_non_matches = 0
        for s_test, s_desired in self.tests:
            if not s_test: continue
            s_clean_text, at_numbers = FwValues.clean_number_words(s_test)
            b_match = s_desired == s_clean_text
            if b_match: continue
            i_non_matches += 1
            print('s_test: "{}"'.format(s_test), b_match)
            print('s_clean_text: "{}"'.format(s_clean_text))
            print('at_numbers:', at_numbers)
            print('')
        assert i_non_matches == 0
    
class TestMultiIndex():
    def test_empty(self):
        result = FwValues.multi_index(None, [])
        assert result == []
    
    def test_string(self):
        result = FwValues.multi_index("a", ['a', 'b', 'c', 'a', 'd', 'b'])
        assert result == [0, 3]
        result = FwValues.multi_index("a", ['a', 'b', 'c', 'a', 'd', 'b'], str)
        assert result == [0, 3]
        result = FwValues.multi_index(None, ['None', 'b', 'c', 'None', 'd', 'b'], str)
        assert result == [0, 3]
    
    def test_float(self):
        result = FwValues.multi_index(1.0, [1.0, 2.0, 3.0, 1.0, 4.0, 2.0])
        assert result == [0, 3]
        result = FwValues.multi_index(1.0, [1.0, 2.0, 3.0, 1.0, 4.0, 2.0], float)
        assert result == [0, 3]
        result = FwValues.multi_index(1, [1.0, 2.0, 3.0, 1.0, 4.0, 2.0], float)
        assert result == [0, 3]
    
    def test_float(self):
        result = FwValues.multi_index(1, [1, 2, 3, 1, 4, 2])
        assert result == [0, 3]
        result = FwValues.multi_index(1, [1, 2, 3, 1, 4, 2], int)
        assert result == [0, 3]
        result = FwValues.multi_index(1.0, [1, 2, 3, 1, 4, 2], int)
        assert result == [0, 3]


class TestGroupContiguousIndexes():
    def test_empty(self):
        result = FwValues.group_contiguous_indexes([])
        assert result == []
        
    def test_normal(self):
        a_test = [1, 2, 3, 6, 7, 9, 11]
        result = FwValues.group_contiguous_indexes(a_test)
        assert result == [[1, 2, 3], [6, 7], [9], [11]]
        
    def test_duplicate(self):
        a_test = [1, 2, 3, 3, 6, 7, 9, 11]
        result = FwValues.group_contiguous_indexes(a_test)
        assert result == [[1, 2, 3, 3], [6, 7], [9], [11]]
        
    def test_unordered(self):
        a_test = [11, 3, 6, 1, 2, 7, 9]
        result = FwValues.group_contiguous_indexes(a_test, b_sort=True)
        assert result == [[1, 2, 3], [6, 7], [9], [11]]


class TestMergeDicts():
    test1 = {'a': 1, 'b':['x'], 'c':{'a': 'ab', 'b': [1, 2]}, 'd':('test', 'this')}
    test2 = {'a': 2, 'b':['y', 'z'], 'c':{'a': 'bc', 'b': [3, 4], 'c': 2.0}, 'd':('out', 'now')}
    test3 = {'a': 'error_01', 'b': {'a': 'error_01', 'b': 'error_02'},}
    test4 = {'a': 'error_07', 'b': {'c': 'error_01', 'b': 'error_04'},}
    
    def test_empty(self):
        assert {} == FwValues.merge_dicts([])
    
    def test_default(self):
        expected = {'a': 3, 'b': ['x', 'y', 'z'], 'c': {'a': 'ab', 'b': [1, 2, 3, 4], 'c': 2.0}, 'd': ('test', 'this')}
        assert expected == FwValues.merge_dicts([self.test1, self.test2])
    
    def test_merge_all(self):
        expected = {'a': 3, 'b': ['x', 'y', 'z'], 'c': {'a': 'abbc', 'b': [1, 2, 3, 4], 'c': 2.0}, 'd': ('test', 'this', 'out', 'now')}
        assert expected == FwValues.merge_dicts(
            [self.test1, self.test2],
            add_numbers=True, concat_strings=True,
            merge_sub_dicts=True, merge_lists=True,
            merge_tuples=True)
    
    def test_anti_default(self):
        expected = {'a': 2, 'b': ['y', 'z'], 'c': {'a': 'bc', 'b': [3, 4], 'c': 2.0}, 'd': ('out', 'now', 'test', 'this')}
        assert expected == FwValues.merge_dicts(
            [self.test1, self.test2],
            add_numbers=False, concat_strings=True,
            merge_sub_dicts=False, merge_lists=False,
            merge_tuples=True, reverse=True)
    
    def test_reversed(self):
        expected = {'a': 3, 'b': ['y', 'z', 'x'], 'c': {'a': 'bc', 'b': [3, 4, 1, 2], 'c': 2.0}, 'd': ('out', 'now')}
        assert expected == FwValues.merge_dicts([self.test1, self.test2], reverse=True)
    
    def test_reversed_no_merge(self):
        expected = {'a': 2, 'b': ['y', 'z'], 'c': {'a': 'bc', 'b': [3, 4], 'c': 2.0}, 'd': ('out', 'now')}
        assert expected == FwValues.merge_dicts(
            [self.test1, self.test2],
            add_numbers=False, concat_strings=False,
            merge_sub_dicts=False, merge_lists=False,
            merge_tuples=False, reverse=True)
    
    def test_merge_str(self):
        expected = {'a': ['error_01', 'error_07'], 'b': {'a': 'error_01', 'b': ['error_02', 'error_04'], 'c': 'error_01'}}
        assert expected == FwValues.merge_dicts(
            [self.test3, self.test4],
            merge_strings=True,
            merge_sub_dicts=True, )


class TestCountChildren():
    variable = [[1, 2, 3, [[1, 2, 3]]], 'a', ({'test': 1, 'this': 2},)]
    def test_include_self(self):
        i_children = FwValues.count_children(self.variable)
        assert 15 == i_children
    def test_not_include_self(self):
        i_children = FwValues.count_children(self.variable, False)
        assert 9 == i_children


class TestCountChildren():
    def test_level_0(self):
        assert 0 == FwValues.child_depth('a')
        assert 0 == FwValues.child_depth('')
        assert 0 == FwValues.child_depth([])
    def test_level_1(self):
        assert 1 == FwValues.child_depth(['a'])
        assert 1 == FwValues.child_depth([''])
        assert 1 == FwValues.child_depth({'a': 2})
    def test_level_2(self):
        assert 2 == FwValues.child_depth((['a'],))
        assert 2 == FwValues.child_depth(([''],))
        assert 2 == FwValues.child_depth(({'a': 2},))
    def test_level_3(self):
        assert 3 == FwValues.child_depth([(['a'],)])
        assert 3 == FwValues.child_depth([([''],)])
        assert 3 == FwValues.child_depth([({'a': 2},)])
    def test_level_4(self):
        assert 4 == FwValues.child_depth([(['a', {'b':1}],)])


class TestDelimitedStrings():
    def test_delimited_strings_to_dicts(self):
        ae_test = FwValues.delimited_strings_to_dicts(["attributeA : value1, attributeB : value2"], s_pairs_delimiter=",", s_key_val_delimiter=":")
        assert ae_test == [{'attributeA': 'value1', 'attributeB': 'value2'}]

class TestAddSuffixKeepLength():
    def test_empty(self):
        assert "_1" == FwValues.add_suffix_keep_length("", 1)
    def test_too_short(self):
        try:
            FwValues.add_suffix_keep_length("", 10, i_max_len=3)
            assert False
        except:
            assert True
    def test_normal(self):
        assert 'tes_100' == FwValues.add_suffix_keep_length("test", 100, s_sep='_', i_max_len=7)

class TestMakeStringsUnique():
    def test_empty(self):
        assert [] == FwValues.make_strings_unique([])
    def test_non_strings(self):
        assert [5, None] == FwValues.make_strings_unique([5, None])
    def test_strings(self):
        a_result = FwValues.make_strings_unique(['-test-', '.test.', '$test$'], 8, lambda x: re.sub(r"[^A-Za-z0-9]", "", x))
        assert ['test', 'test_1', 'test_2'] == a_result


class TestMeanOfProducts():
    def test_both_empty(self):
        assert 0.0 == FwValues.mean_of_products([], [])
    def test_one_empty(self):
        assert 0.0 == FwValues.mean_of_products([], [1])
        assert 0.0 == FwValues.mean_of_products([1], [])
    def test_regular(self):
        assert 6.0 == FwValues.mean_of_products([2], [3])


class TestMovingAverage():
    vals = [1,2,3,4,3,2,1]
    def test_empty(self):
        assert [] == FwValues.moving_average([])
    def test_single(self):
        assert [5] == FwValues.moving_average([5])
    def test_default(self):
        a_expected = [1.0, 1.5, 2.25, 3.125, 3.0625, 2.53125, 1.765625]
        assert FwValues.moving_average(self.vals) == a_expected
    def test_low(self):
        a_expected = [1.0, 1.75, 2.6875, 3.671875, 3.16796875, 2.2919921875, 1.322998046875]
        assert FwValues.moving_average(self.vals, 0.25) == a_expected
    def test_high(self):
        a_expected = [1.0, 1.25, 1.6875, 2.265625, 2.44921875, 2.3369140625, 2.002685546875]
        assert FwValues.moving_average(self.vals, 0.75) == a_expected


class TestSmooth():
    vals = [1,2,3,4,3,2,1]
    def test_empty(self):
        assert [] == FwValues.smooth([])
    def test_single(self):
        assert [5] == FwValues.smooth([5])
    def test_default(self):
        a_expected = [1.38, 2.02, 2.66, 3.12, 2.66, 2.02, 1.38]
        assert FwValues.smooth(self.vals) == a_expected
    def test_low(self):
        a_expected = [1.16, 2.02, 2.93, 3.67, 2.93, 2.02, 1.16]
        assert FwValues.smooth(self.vals, 0.25) == a_expected
    def test_high(self):
        a_expected = [1.5, 1.79, 2.07, 2.27, 2.07, 1.79, 1.5]
        assert FwValues.smooth(self.vals, 0.75) == a_expected


class TestPearsonCorrelation():    
    def test_empty(self):
        assert 0.0 == FwValues.pearson_correlation([], [])
    def test_single(self):
        assert 0.0 == FwValues.pearson_correlation([5], [10])
    def test_zeroes(self):
        assert 0.0 == FwValues.pearson_correlation([1, 2], [0, 0])
    def test_normal(self):
        assert 1.0 == FwValues.pearson_correlation([1, 2], [10, 11])
    def test_inverse(self):
        assert -1.0 == FwValues.pearson_correlation([2, 1], [10, 11])
    def test_not(self):
        assert 0.0 == FwValues.pearson_correlation([1, 2], [10, 10])
    def test_mismatch(self):
        assert 1.0 == FwValues.pearson_correlation([1, 2], [10, 11, 12])
        assert 1.0 == FwValues.pearson_correlation([1, 2, 3], [10, 11])


#class TestVectors():
#need to write unit tests for vectors
