cython
numpy
scikit-learn
scipy
# Dependencies for development and test environment
pytest
pytest-cov
pylint
