import re, string
import numpy as np
from numpy import isnan

__all__ = ["count_sizes", "count_colors", "count_links", "count_images", "count_letters",
           "count_numbers", "count_currency", "count_percents", "count_words"]

e_empty = set(['', '0', 0, np.nan])
re_clothes_size = re.compile(
    r"\b(XS|X-?Small|S|Sm|Small|M|Me?d|Medium|L|Lg|Large|(X{1,6}|Extra)[ -]?L(arge|g)?|sizes?[:=])\b",
    re.I)

a_colors = ['black', 'white', 'gray', 'grey', 'gold', 'silver', 'charcoal'
            'red', 'orange', 'yellow', 'green', 'blue', 'indigo', 'violet',
            'purple', 'navy', 'olive', 'pink',
            'brown', 'tan', 'khaki', 'nude', 'ivory', 'taupe',
            'cyan', 'teal', 'aqua', 'turquoise', 'magenta']
re_color = re.compile(r"\b({})\b".format('|'.join(a_colors)), re.I)

re_links = re.compile(r"(www\.[^ \t\r\n]+|http:[^ \t\r\n]+)( |$)", re.I)
re_images = re.compile(r"\.(png|jpg|jpeg|gif|bmp)", re.I)
re_letters = re.compile(r"[a-z]", re.I)
re_numbers = re.compile(r"(\d+,)*\d+(\.\d+)?")
re_currency = re.compile(r"[�$](\d+,)*\d+(\.\d+)?")
re_percents = re.compile(r"(\d+,)*\d+(\.\d+)?%")
re_taxonomy = re.compile(r">")


cpdef int count_sizes(str s_text):
    ''' Count size patterns in s_text '''
    return len(re_clothes_size.findall(s_text))
    
cpdef int count_colors(str s_text):
    ''' Count standard colors in s_text '''
    return len(re_color.findall(s_text))
    
cpdef int count_links(str s_text):
    ''' Count link patterns in s_text '''
    return len(re_links.findall(s_text))
    
cpdef int count_images(str s_text):
    ''' Count image patterns in s_text '''
    return len(re_images.findall(s_text))
    
cpdef int count_letters(str s_text):
    ''' Count letters in s_text '''
    return len(re_letters.findall(s_text))

cpdef int count_numbers(str s_text):
    ''' Count numbers in s_text '''
    return len(re_numbers.findall(s_text))

cpdef int count_currency(str s_text):
    ''' Count currency in s_text '''
    return len(re_currency.findall(s_text))

cpdef int count_percents(str s_text):
    ''' Count percents in s_text '''
    return len(re_percents.findall(s_text))
    
cpdef int count_words(str s_text):
    ''' Count words in s_text '''
    return len(word_tokenize(s_text))
    

# ------------ utility ------------
cpdef bint is_empty(v_val):
    if v_val is None:
        return True
    elif isinstance(v_val, int):
        return False
    elif isinstance(v_val, float):
        if isnan(v_val):
            return True
        elif np.isnan(v_val):
            return True
        else:
            return False
    elif isinstance(v_val, str):
        s_val = v_val.strip()
        if len(s_val) == 0:
            return True
        elif s_val == '0':
            return True
        else:
            return False
    else:
        return False


cpdef list word_tokenize(text, bint remove_punct=True):
    """ Simple tokenizer that splits on spaces.
        Can remove punctuation. """
    if remove_punct:
        pattern = "[" + re.escape(string.punctuation) + "]"
        text = re.sub(pattern, "", text)

    return text.split()
