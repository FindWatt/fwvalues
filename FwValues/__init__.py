import pyximport; pyximport.install()
try:
    from values import *
except:
    from .values import *
try:
    from metadata import *
except:
    from .metadata import *

__version__ = "1.2.0"