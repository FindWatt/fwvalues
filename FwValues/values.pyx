import re, math
import numpy as np
from sklearn.decomposition import PCA
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as LDA

__all__ = [
    "is_empty", "num_to_string",
    "list_values", "fraction_to_float", "to_float",
    "NumberSet",
    "num_word_value", "get_number_piece_values", "get_number_values", "_get_key_regex_sort", "clean_number_words",
    "list_numbers", "value_list_sum", "value_list_avg",
    "value_list_stddev", "value_list_herfindahl",
    "value_list_similarity", "value_list_variation", "multi_index",
    "group_contiguous_indexes", "merge_dicts", "count_children", "child_depth",
    "add_suffix_keep_length", "make_strings_unique",
    "delimited_strings_to_dicts",
    "mean_of_products", "pearson_correlation", "moving_average", "smooth",
    "vector_similarity", "average_vectors", "filter_vectors", "plottable_vectors",
]

re_DIGITS = re.compile('\d+')
re_NONDIGITS = re.compile('[^\d.]+')
re_NONDIGITS_SIGNS = re.compile('[^\d./ +-]+')

re_FRACTION = re.compile(r'(?P<sign>[+-]?)((?P<whole>\d+)([ +-]| [+-] ))?(?P<num>\d+)/(?P<denom>[1-9]\d*)')
re_DECIMAL = re.compile(r'(?P<sign>[+-]?)(?P<thousands>(\d{1,6},)+)?(?P<whole>(?<!\.)\d+)?(?P<dec>\.\d+)?(?P<percent> ?%)?')

cdef set c_DIGITS = set('0123456789')

a_DIGIT_WORDS = [
    'zero', 'one', 'two', 'three', 'four', 'five',
    'six', 'seven', 'eight', 'nine',]
cdef set c_DIGIT_WORDS = set(a_DIGIT_WORDS)

a_TENS_WORDS = [
    'twenty', 'thirty', 'fourty', 'fifty',
    'sixty', 'seventy', 'eighty', 'ninety']
cdef set c_TENS_WORDS = set(a_TENS_WORDS)

e_NUMBER_WORD_EXCEPTIONS = {
    (240, 7): '24/7',
    (9, 1, 1): '911',
    ('nine', 'one', 'one'): '911',
    ('four', 'one', 'one'): '911',
    (9, 11): '9/11',
}

re_SPELLED_OUT_NUMBER = re.compile( # for finding numbers that are spelled out digit by digit
    r"\b((zero|one|two|three|four|five|six|seven|eight|nine)[-_ ]){2,}",
    flags=re.I)

re_OUTER_NUMBER_PATTERN = re.compile(  #for finding a phrase made of one or more number words
    r"\b(\+-)?"
    r"((\d+(\.\d+)?|zero|one|two|three|four\b|five|six\b|seven\b|eight\b|nine\b|ten\b|"
    r"eleven|twelve|thirteen\b|fourteen\b|fifteen\b|sixteen\b|seventeen\b|eighteen\b|nineteen\b|"
    r"twenty|thirty|forty|fifty|sixty|seventy|eighty|ninety|hundreds?\b|"
    r"thousands?\b|millions?\b|billions?\b|trillions?\b|quadrillions?\b|quintillions?\b|"
    r"dozen|score|gross|a\b|no|and|minus|plus|negative|positive|point|dot|"
    r"half|halve|first|second|third|fourth|fifth|sixth|seventh|eighth|ninth|tenth|twelfth|"
    r"(thir|four|fif|six|seven|eigh|nine)teenth|"
    r"(twen|thir|four|fif|six|seven|eigh|nine)ieth|"
    r"thirty[ -]*second(th)?|sixty[ -]*fourth|hundredth|thousandth|millionth|billionth"
    r")s?[-_ /&,]*)+",
    flags=re.I)
    
re_OUTER_NUMBER_PATTERN_CONSTRAINED = re.compile(  #for finding a phrase made of one or more number words
    r"\b(\+-)?"
    r"(zero|one|two|three|four|five|six|seven|eight|nine|ten|"
    r"eleven|twelve|thirteen|fourteen|fifteen|sixteen|seventeen|eighteen|nineteen|"
    r"twenty|thirty|forty|fifty|sixty|seventy|eighty|ninety)"
    r"([-_ /,]+"
    r"((hundred|thousand|(m|b|tr|quadr|quint)illion)(th)?|"
    r"dozen|score|gross|half|halve|"
    r"first|second|third|fourth|fifth|sixth|seventh|eighth|ninth|tenth|eleventh|twelfth|"
    r"(thir|four|fif|six|seven|eigh|nine)teenth|"
    r"(twen|thir|four|fif|six|seven|eigh|nine)ieth|"
    r"thirty[ -]*second(th)?|sixty[ -]*fourth"
    r")s?)?",
    flags=re.I)

re_NUMBER_PATTERN = re.compile(  # for calculating the value of identifiable number phrase components
    r"\b"
    r"(?P<sign>negative[ _-]*|positive[ _-]*|minus[ _-]*|plus[ _-]*|less[ _-]*|-)?"
    r"(?P<decimal>[ _-]*(point|dot)[ _-]*"
        r"(?P<decimalzeroes>(zero[ _-]*)*))?"
    r"(?P<prefix>twenty|thirty|forty|fifty|sixty|seventy|eighty|ninety|"
    r"one|two|three|four\b|five|six\b|seven\b|eight\b|nine\b|ten|"
    r"eleven|twelve|thir|fif)?[ _-]*"
    r"(?P<number>\d(\.\d+)?)?"
    r"(?P<digit>(zero|one|two|three|four\b|five|six\b|seven\b|eight\b|nine\b|teen\b|a\b|no\b))?"
    r"(?P<suffix>[ _-]*(dozen|score|gross|hundreds?\b|thousands?\b|millions?\b|billions?\b|trillions?\b))?"
    r"([ _-]*(and[ _-]*)?"
        r"(?P<fraction>(?P<numerator>a|zero|no|one|two|three|four\b|five\b|six\b|seven\b|eight\b|nine\b|ten\b)?[- /_]*"
        r"(?P<denominator>half|halve|"
        r"(((twenty|thirty|forty|fifty|sixty|seventy|eighty|ninety)?[-_ ]*)(first|second|third|fourth|fifth|sixth|seventh|eighth|ninth))|"
        r"tenth|twelfth|"
        r"(thir|four|fif|six|seven|eigh|nine)teenth|"
        r"thirty[ -]*second(th)?|sixty[ -]*fourth|hundredth|thousandth|(m|b|tr|quadr|quint)illionth)s?))?",
    flags=re.I)

e_NUMBER_VALUES = {
    "zero": 0, "one": 1, "two": 2, "three": 3, "four": 4, "five": 5,
    "six": 6, "seven": 7, "eight": 8, "nine": 9, "ten": 10,
    "eleven": 11, "twelve": 12, "thir": 3, "fif": 5, "teen": 10,
    "twenty": 20, "thirty": 30, "forty": 40, "fifty": 50,
    "sixty": 60, "seventy": 70, "eighty": 80, "ninety": 90,
    "hundred": 100, "hundreds": 100,
    "thousand": 1000, "thousands": 1000,
    "million": 1000000, "millions": 1000000,
    "billion": 1000000000, "billions": 1000000000,
    "trillion": 1000000000000, "trillions": 1000000000000,
    "quadrillion": 1000000000000, "quadrillions": 1000000000000,
    "dozen": 12, "dozens": 12,
    "score": 20, "scores": 20,
    "gross": 144,
    "a": 1, "no": 0,
    "half": 1.0/2, "halve": 1.0/2, "halfs": 1.0/2, "halves": 1.0/2,
    "third": 1.0/3, "thirds": 1.0/3,
    "fourth": 1.0/4, "fourths": 1.0/4,
    "fifth": 1.0/5, "fifths": 1.0/5,
    "sixth": 1.0/6, "sixths": 1.0/6,
    "seventh": 1.0/7, "sevenths": 1.0/7,
    "eighth": 1.0/8, "eighths": 1.0/8,
    "ninth": 1.0/9, "ninths": 1.0/9,
    "tenth": 1.0/10, "tenths": 1.0/10,
    "eleventh": 1.0/11, "elevenths": 1.0/11,
    "twelfth": 1.0/12, "twelfths": 1.0/12,
    "thirteenth": 1.0/13, "thirteenths": 1.0/13,
    "fourteenth": 1.0/13, "fourteenths": 1.0/14,
    "fifteenth": 1.0/13, "fifteenths": 1.0/15,
    "sixteenth": 1.0/16, "sixteenths": 1.0/16,
    "seventeenth": 1.0/13, "seventeenths": 1.0/17,
    "eighteenth": 1.0/13, "eighteenth": 1.0/18,
    "nineteenth": 1.0/13, "nineteenths": 1.0/19,
    "twentieth": 1.0/20, "twentieths": 1.0/20,
    "thirtieth": 1.0/30, "thirtieths": 1.0/30,
    "fourtieth": 1.0/40, "fourtieths": 1.0/40,
    "fiftieth": 1.0/50, "fiftieths": 1.0/50,
    "sixtieth": 1.0/60, "sixtieths": 1.0/60,
    "seventieth": 1.0/70, "seventieths": 1.0/70,
    "eightieth": 1.0/80, "eightieths": 1.0/80,
    "ninetieth": 1.0/90, "ninetieths": 1.0/90,
    "hundredth": 0.01, "hundredths": 0.01,
    "thousandth": 0.001, "thousandths": 0.001,
    "millionth": 0.000001, "millionths": 0.000001,
    "billionth": 0.000000001, "billionths": 0.000000001,
    "trillionth": 0.000000000001, "trillionths": 0.000000000001,
    "twentyfirst": 1.0/21, "twentyfirsts": 1.0/21, "twenty first": 1.0/21, "twenty firsts": 1.0/21,
    "twenty-first": 1.0/21, "twenty-firsts": 1.0/21, "twenty_first": 1.0/21, "twenty_firsts": 1.0/21,
    "twentysecond": 1.0/22, "twentyseconds": 1.0/22, "twenty second": 1.0/22, "twenty seconds": 1.0/22,
    "twenty-second": 1.0/22, "twenty-seconds": 1.0/22, "twenty_second": 1.0/22, "twenty_seconds": 1.0/22,
    "twentysecondth": 1.0/22, "twentysecondths": 1.0/22, "twenty secondth": 1.0/22, "twenty secondths": 1.0/22,
    "twenty-secondth": 1.0/22, "twenty-secondths": 1.0/22, "twenty_secondth": 1.0/22, "twenty_secondths": 1.0/22,
    "twentythird": 1.0/23, "twentythirds": 1.0/23, "twenty third": 1.0/23, "twenty thirds": 1.0/23,
    "twenty-third": 1.0/23, "twenty-thirds": 1.0/23, "twenty_third": 1.0/23, "twenty_thirds": 1.0/23,
    "twentyfourth": 1.0/24, "twentyfourths": 1.0/24, "twenty fourth": 1.0/24, "twenty fourths": 1.0/24,
    "twenty-fourth": 1.0/24, "twenty-fourths": 1.0/24, "twenty_fourth": 1.0/24, "twenty_fourths": 1.0/24,
    "twentyfifth": 1.0/25, "twentyfifths": 1.0/25, "twenty fifth": 1.0/25, "twenty fifths": 1.0/25,
    "twenty-fifth": 1.0/25, "twenty-fifths": 1.0/25, "twenty_fifth": 1.0/25, "twenty_fifths": 1.0/25,
    "twentysixth": 1.0/26, "twentysixths": 1.0/26, "twenty sixth": 1.0/26, "twenty sixths": 1.0/26,
    "twenty-sixth": 1.0/26, "twenty-sixths": 1.0/26, "twenty_sixth": 1.0/26, "twenty_sixths": 1.0/26,
    "twentyseventh": 1.0/27, "twentysevenths": 1.0/27, "twenty seventh": 1.0/27, "twenty sevenths": 1.0/27,
    "twenty-seventh": 1.0/27, "twenty-sevenths": 1.0/27, "twenty_seventh": 1.0/27, "twenty_sevenths": 1.0/27,
    "twentyeighth": 1.0/28, "twentyeighths": 1.0/28, "twenty eighth": 1.0/28, "twenty eighths": 1.0/28,
    "twenty-eighth": 1.0/28, "twenty-eighths": 1.0/28, "twenty_eighth": 1.0/28, "twenty_eighths": 1.0/28,
    "twentyninth": 1.0/29, "twentyninths": 1.0/29, "twenty ninth": 1.0/29, "twenty ninths": 1.0/29,
    "twenty-ninth": 1.0/29, "twenty-ninths": 1.0/29, "twenty_ninth": 1.0/29, "twenty_ninths": 1.0/29,
    "thirtyfirst": 1.0/31, "thirtyfirsts": 1.0/31, "thirty first": 1.0/31, "thirty firsts": 1.0/31,
    "thirty-first": 1.0/31, "thirty-firsts": 1.0/31, "thirty_first": 1.0/31, "thirty_firsts": 1.0/31,
    "thirtysecond": 1.0/32, "thirtyseconds": 1.0/32, "thirty second": 1.0/32, "thirty seconds": 1.0/32,
    "thirty-second": 1.0/32, "thirty-seconds": 1.0/32, "thirty_second": 1.0/32, "thirty_seconds": 1.0/32,
    "thirtysecondth": 1.0/32, "thirtysecondths": 1.0/32, "thirty secondth": 1.0/32, "thirty secondths": 1.0/32,
    "thirty-secondth": 1.0/32, "thirty-secondths": 1.0/32, "thirty_secondth": 1.0/32, "thirty_secondths": 1.0/32,
    "thirtythird": 1.0/33, "thirtythirds": 1.0/33, "thirty third": 1.0/33, "thirty thirds": 1.0/33,
    "thirty-third": 1.0/33, "thirty-thirds": 1.0/33, "thirty_third": 1.0/33, "thirty_thirds": 1.0/33,
    "thirtyfourth": 1.0/34, "thirtyfourths": 1.0/34, "thirty fourth": 1.0/34, "thirty fourths": 1.0/34,
    "thirty-fourth": 1.0/34, "thirty-fourths": 1.0/34, "thirty_fourth": 1.0/34, "thirty_fourths": 1.0/34,
    "thirtyfifth": 1.0/35, "thirtyfifths": 1.0/35, "thirty fifth": 1.0/35, "thirty fifths": 1.0/35,
    "thirty-fifth": 1.0/35, "thirty-fifths": 1.0/35, "thirty_fifth": 1.0/35, "thirty_fifths": 1.0/35,
    "thirtysixth": 1.0/36, "thirtysixths": 1.0/36, "thirty sixth": 1.0/36, "thirty sixths": 1.0/36,
    "thirty-sixth": 1.0/36, "thirty-sixths": 1.0/36, "thirty_sixth": 1.0/36, "thirty_sixths": 1.0/36,
    "thirtyseventh": 1.0/37, "thirtysevenths": 1.0/37, "thirty seventh": 1.0/37, "thirty sevenths": 1.0/37,
    "thirty-seventh": 1.0/37, "thirty-sevenths": 1.0/37, "thirty_seventh": 1.0/37, "thirty_sevenths": 1.0/37,
    "thirtyeighth": 1.0/38, "thirtyeighths": 1.0/38, "thirty eighth": 1.0/38, "thirty eighths": 1.0/38,
    "thirty-eighth": 1.0/38, "thirty-eighths": 1.0/38, "thirty_eighth": 1.0/38, "thirty_eighths": 1.0/38,
    "thirtyninth": 1.0/39, "thirtyninths": 1.0/39, "thirty ninth": 1.0/39, "thirty ninths": 1.0/39,
    "thirty-ninth": 1.0/39, "thirty-ninths": 1.0/39, "thirty_ninth": 1.0/39, "thirty_ninths": 1.0/39,
    "fortyfirst": 1.0/41, "fortyfirsts": 1.0/41, "forty first": 1.0/41, "forty firsts": 1.0/41,
    "forty-first": 1.0/41, "forty-firsts": 1.0/41, "forty_first": 1.0/41, "forty_firsts": 1.0/41,
    "fortysecond": 1.0/42, "fortyseconds": 1.0/42, "forty second": 1.0/42, "forty seconds": 1.0/42,
    "forty-second": 1.0/42, "forty-seconds": 1.0/42, "forty_second": 1.0/42, "forty_seconds": 1.0/42,
    "fortysecondth": 1.0/42, "fortysecondths": 1.0/42, "forty secondth": 1.0/42, "forty secondths": 1.0/42,
    "forty-secondth": 1.0/42, "forty-secondths": 1.0/42, "forty_secondth": 1.0/42, "forty_secondths": 1.0/42,
    "fortythird": 1.0/43, "fortythirds": 1.0/43, "forty third": 1.0/43, "forty thirds": 1.0/43,
    "forty-third": 1.0/43, "forty-thirds": 1.0/43, "forty_third": 1.0/43, "forty_thirds": 1.0/43,
    "fortyfourth": 1.0/44, "fortyfourths": 1.0/44, "forty fourth": 1.0/44, "forty fourths": 1.0/44,
    "forty-fourth": 1.0/44, "forty-fourths": 1.0/44, "forty_fourth": 1.0/44, "forty_fourths": 1.0/44,
    "fortyfifth": 1.0/45, "fortyfifths": 1.0/45, "forty fifth": 1.0/45, "forty fifths": 1.0/45,
    "forty-fifth": 1.0/45, "forty-fifths": 1.0/45, "forty_fifth": 1.0/45, "forty_fifths": 1.0/45,
    "fortysixth": 1.0/46, "fortysixths": 1.0/46, "forty sixth": 1.0/46, "forty sixths": 1.0/46,
    "forty-sixth": 1.0/46, "forty-sixths": 1.0/46, "forty_sixth": 1.0/46, "forty_sixths": 1.0/46,
    "fortyseventh": 1.0/47, "fortysevenths": 1.0/47, "forty seventh": 1.0/47, "forty sevenths": 1.0/47,
    "forty-seventh": 1.0/47, "forty-sevenths": 1.0/47, "forty_seventh": 1.0/47, "forty_sevenths": 1.0/47,
    "fortyeighth": 1.0/48, "fortyeighths": 1.0/48, "forty eighth": 1.0/48, "forty eighths": 1.0/48,
    "forty-eighth": 1.0/48, "forty-eighths": 1.0/48, "forty_eighth": 1.0/48, "forty_eighths": 1.0/48,
    "fortyninth": 1.0/49, "fortyninths": 1.0/49, "forty ninth": 1.0/49, "forty ninths": 1.0/49,
    "forty-ninth": 1.0/49, "forty-ninths": 1.0/49, "forty_ninth": 1.0/49, "forty_ninths": 1.0/49,
    "fiftyfirst": 1.0/51, "fiftyfirsts": 1.0/51, "fifty first": 1.0/51, "fifty firsts": 1.0/51,
    "fifty-first": 1.0/51, "fifty-firsts": 1.0/51, "fifty_first": 1.0/51, "fifty_firsts": 1.0/51,
    "fiftysecond": 1.0/52, "fiftyseconds": 1.0/52, "fifty second": 1.0/52, "fifty seconds": 1.0/52,
    "fifty-second": 1.0/52, "fifty-seconds": 1.0/52, "fifty_second": 1.0/52, "fifty_seconds": 1.0/52,
    "fiftysecondth": 1.0/52, "fiftysecondths": 1.0/52, "fifty secondth": 1.0/52, "fifty secondths": 1.0/52,
    "fifty-secondth": 1.0/52, "fifty-secondths": 1.0/52, "fifty_secondth": 1.0/52, "fifty_secondths": 1.0/52,
    "fiftythird": 1.0/53, "fiftythirds": 1.0/53, "fifty third": 1.0/53, "fifty thirds": 1.0/53,
    "fifty-third": 1.0/53, "fifty-thirds": 1.0/53, "fifty_third": 1.0/53, "fifty_thirds": 1.0/53,
    "fiftyfourth": 1.0/54, "fiftyfourths": 1.0/54, "fifty fourth": 1.0/54, "fifty fourths": 1.0/54,
    "fifty-fourth": 1.0/54, "fifty-fourths": 1.0/54, "fifty_fourth": 1.0/54, "fifty_fourths": 1.0/54,
    "fiftyfifth": 1.0/55, "fiftyfifths": 1.0/55, "fifty fifth": 1.0/55, "fifty fifths": 1.0/55,
    "fifty-fifth": 1.0/55, "fifty-fifths": 1.0/55, "fifty_fifth": 1.0/55, "fifty_fifths": 1.0/55,
    "fiftysixth": 1.0/56, "fiftysixths": 1.0/56, "fifty sixth": 1.0/56, "fifty sixths": 1.0/56,
    "fifty-sixth": 1.0/56, "fifty-sixths": 1.0/56, "fifty_sixth": 1.0/56, "fifty_sixths": 1.0/56,
    "fiftyseventh": 1.0/57, "fiftysevenths": 1.0/57, "fifty seventh": 1.0/57, "fifty sevenths": 1.0/57,
    "fifty-seventh": 1.0/57, "fifty-sevenths": 1.0/57, "fifty_seventh": 1.0/57, "fifty_sevenths": 1.0/57,
    "fiftyeighth": 1.0/58, "fiftyeighths": 1.0/58, "fifty eighth": 1.0/58, "fifty eighths": 1.0/58,
    "fifty-eighth": 1.0/58, "fifty-eighths": 1.0/58, "fifty_eighth": 1.0/58, "fifty_eighths": 1.0/58,
    "fiftyninth": 1.0/59, "fiftyninths": 1.0/59, "fifty ninth": 1.0/59, "fifty ninths": 1.0/59,
    "fifty-ninth": 1.0/59, "fifty-ninths": 1.0/59, "fifty_ninth": 1.0/59, "fifty_ninths": 1.0/59,
    "sixtyfirst": 1.0/61, "sixtyfirsts": 1.0/61, "sixty first": 1.0/61, "sixty firsts": 1.0/61,
    "sixty-first": 1.0/61, "sixty-firsts": 1.0/61, "sixty_first": 1.0/61, "sixty_firsts": 1.0/61,
    "sixtysecond": 1.0/62, "sixtyseconds": 1.0/62, "sixty second": 1.0/62, "sixty seconds": 1.0/62,
    "sixty-second": 1.0/62, "sixty-seconds": 1.0/62, "sixty_second": 1.0/62, "sixty_seconds": 1.0/62,
    "sixtysecondth": 1.0/62, "sixtysecondths": 1.0/62, "sixty secondth": 1.0/62, "sixty secondths": 1.0/62,
    "sixty-secondth": 1.0/62, "sixty-secondths": 1.0/62, "sixty_secondth": 1.0/62, "sixty_secondths": 1.0/62,
    "sixtythird": 1.0/63, "sixtythirds": 1.0/63, "sixty third": 1.0/63, "sixty thirds": 1.0/63,
    "sixty-third": 1.0/63, "sixty-thirds": 1.0/63, "sixty_third": 1.0/63, "sixty_thirds": 1.0/63,
    "sixtyfourth": 1.0/64, "sixtyfourths": 1.0/64, "sixty fourth": 1.0/64, "sixty fourths": 1.0/64,
    "sixty-fourth": 1.0/64, "sixty-fourths": 1.0/64, "sixty_fourth": 1.0/64, "sixty_fourths": 1.0/64,
    "sixtyfifth": 1.0/65, "sixtyfifths": 1.0/65, "sixty fifth": 1.0/65, "sixty fifths": 1.0/65,
    "sixty-fifth": 1.0/65, "sixty-fifths": 1.0/65, "sixty_fifth": 1.0/65, "sixty_fifths": 1.0/65,
    "sixtysixth": 1.0/66, "sixtysixths": 1.0/66, "sixty sixth": 1.0/66, "sixty sixths": 1.0/66,
    "sixty-sixth": 1.0/66, "sixty-sixths": 1.0/66, "sixty_sixth": 1.0/66, "sixty_sixths": 1.0/66,
    "sixtyseventh": 1.0/67, "sixtysevenths": 1.0/67, "sixty seventh": 1.0/67, "sixty sevenths": 1.0/67,
    "sixty-seventh": 1.0/67, "sixty-sevenths": 1.0/67, "sixty_seventh": 1.0/67, "sixty_sevenths": 1.0/67,
    "sixtyeighth": 1.0/68, "sixtyeighths": 1.0/68, "sixty eighth": 1.0/68, "sixty eighths": 1.0/68,
    "sixty-eighth": 1.0/68, "sixty-eighths": 1.0/68, "sixty_eighth": 1.0/68, "sixty_eighths": 1.0/68,
    "sixtyninth": 1.0/69, "sixtyninths": 1.0/69, "sixty ninth": 1.0/69, "sixty ninths": 1.0/69,
    "sixty-ninth": 1.0/69, "sixty-ninths": 1.0/69, "sixty_ninth": 1.0/69, "sixty_ninths": 1.0/69,
    "seventyfirst": 1.0/71, "seventyfirsts": 1.0/71, "seventy first": 1.0/71, "seventy firsts": 1.0/71,
    "seventy-first": 1.0/71, "seventy-firsts": 1.0/71, "seventy_first": 1.0/71, "seventy_firsts": 1.0/71,
    "seventysecond": 1.0/72, "seventyseconds": 1.0/72, "seventy second": 1.0/72, "seventy seconds": 1.0/72,
    "seventy-second": 1.0/72, "seventy-seconds": 1.0/72, "seventy_second": 1.0/72, "seventy_seconds": 1.0/72,
    "seventysecondth": 1.0/72, "seventysecondths": 1.0/72, "seventy secondth": 1.0/72, "seventy secondths": 1.0/72,
    "seventy-secondth": 1.0/72, "seventy-secondths": 1.0/72, "seventy_secondth": 1.0/72, "seventy_secondths": 1.0/72,
    "seventythird": 1.0/73, "seventythirds": 1.0/73, "seventy third": 1.0/73, "seventy thirds": 1.0/73,
    "seventy-third": 1.0/73, "seventy-thirds": 1.0/73, "seventy_third": 1.0/73, "seventy_thirds": 1.0/73,
    "seventyfourth": 1.0/74, "seventyfourths": 1.0/74, "seventy fourth": 1.0/74, "seventy fourths": 1.0/74,
    "seventy-fourth": 1.0/74, "seventy-fourths": 1.0/74, "seventy_fourth": 1.0/74, "seventy_fourths": 1.0/74,
    "seventyfifth": 1.0/75, "seventyfifths": 1.0/75, "seventy fifth": 1.0/75, "seventy fifths": 1.0/75,
    "seventy-fifth": 1.0/75, "seventy-fifths": 1.0/75, "seventy_fifth": 1.0/75, "seventy_fifths": 1.0/75,
    "seventysixth": 1.0/76, "seventysixths": 1.0/76, "seventy sixth": 1.0/76, "seventy sixths": 1.0/76,
    "seventy-sixth": 1.0/76, "seventy-sixths": 1.0/76, "seventy_sixth": 1.0/76, "seventy_sixths": 1.0/76,
    "seventyseventh": 1.0/77, "seventysevenths": 1.0/77, "seventy seventh": 1.0/77, "seventy sevenths": 1.0/77,
    "seventy-seventh": 1.0/77, "seventy-sevenths": 1.0/77, "seventy_seventh": 1.0/77, "seventy_sevenths": 1.0/77,
    "seventyeighth": 1.0/78, "seventyeighths": 1.0/78, "seventy eighth": 1.0/78, "seventy eighths": 1.0/78,
    "seventy-eighth": 1.0/78, "seventy-eighths": 1.0/78, "seventy_eighth": 1.0/78, "seventy_eighths": 1.0/78,
    "seventyninth": 1.0/79, "seventyninths": 1.0/79, "seventy ninth": 1.0/79, "seventy ninths": 1.0/79,
    "seventy-ninth": 1.0/79, "seventy-ninths": 1.0/79, "seventy_ninth": 1.0/79, "seventy_ninths": 1.0/79,
    "eightyfirst": 1.0/81, "eightyfirsts": 1.0/81, "eighty first": 1.0/81, "eighty firsts": 1.0/81,
    "eighty-first": 1.0/81, "eighty-firsts": 1.0/81, "eighty_first": 1.0/81, "eighty_firsts": 1.0/81,
    "eightysecond": 1.0/82, "eightyseconds": 1.0/82, "eighty second": 1.0/82, "eighty seconds": 1.0/82,
    "eighty-second": 1.0/82, "eighty-seconds": 1.0/82, "eighty_second": 1.0/82, "eighty_seconds": 1.0/82,
    "eightysecondth": 1.0/82, "eightysecondths": 1.0/82, "eighty secondth": 1.0/82, "eighty secondths": 1.0/82,
    "eighty-secondth": 1.0/82, "eighty-secondths": 1.0/82, "eighty_secondth": 1.0/82, "eighty_secondths": 1.0/82,
    "eightythird": 1.0/83, "eightythirds": 1.0/83, "eighty third": 1.0/83, "eighty thirds": 1.0/83,
    "eighty-third": 1.0/83, "eighty-thirds": 1.0/83, "eighty_third": 1.0/83, "eighty_thirds": 1.0/83,
    "eightyfourth": 1.0/84, "eightyfourths": 1.0/84, "eighty fourth": 1.0/84, "eighty fourths": 1.0/84,
    "eighty-fourth": 1.0/84, "eighty-fourths": 1.0/84, "eighty_fourth": 1.0/84, "eighty_fourths": 1.0/84,
    "eightyfifth": 1.0/85, "eightyfifths": 1.0/85, "eighty fifth": 1.0/85, "eighty fifths": 1.0/85,
    "eighty-fifth": 1.0/85, "eighty-fifths": 1.0/85, "eighty_fifth": 1.0/85, "eighty_fifths": 1.0/85,
    "eightysixth": 1.0/86, "eightysixths": 1.0/86, "eighty sixth": 1.0/86, "eighty sixths": 1.0/86,
    "eighty-sixth": 1.0/86, "eighty-sixths": 1.0/86, "eighty_sixth": 1.0/86, "eighty_sixths": 1.0/86,
    "eightyseventh": 1.0/87, "eightysevenths": 1.0/87, "eighty seventh": 1.0/87, "eighty sevenths": 1.0/87,
    "eighty-seventh": 1.0/87, "eighty-sevenths": 1.0/87, "eighty_seventh": 1.0/87, "eighty_sevenths": 1.0/87,
    "eightyeighth": 1.0/88, "eightyeighths": 1.0/88, "eighty eighth": 1.0/88, "eighty eighths": 1.0/88,
    "eighty-eighth": 1.0/88, "eighty-eighths": 1.0/88, "eighty_eighth": 1.0/88, "eighty_eighths": 1.0/88,
    "eightyninth": 1.0/89, "eightyninths": 1.0/89, "eighty ninth": 1.0/89, "eighty ninths": 1.0/89,
    "eighty-ninth": 1.0/89, "eighty-ninths": 1.0/89, "eighty_ninth": 1.0/89, "eighty_ninths": 1.0/89,
    "ninetyfirst": 1.0/91, "ninetyfirsts": 1.0/91, "ninety first": 1.0/91, "ninety firsts": 1.0/91,
    "ninety-first": 1.0/91, "ninety-firsts": 1.0/91, "ninety_first": 1.0/91, "ninety_firsts": 1.0/91,
    "ninetysecond": 1.0/92, "ninetyseconds": 1.0/92, "ninety second": 1.0/92, "ninety seconds": 1.0/92,
    "ninety-second": 1.0/92, "ninety-seconds": 1.0/92, "ninety_second": 1.0/92, "ninety_seconds": 1.0/92,
    "ninetysecondth": 1.0/92, "ninetysecondths": 1.0/92, "ninety secondth": 1.0/92, "ninety secondths": 1.0/92,
    "ninety-secondth": 1.0/92, "ninety-secondths": 1.0/92, "ninety_secondth": 1.0/92, "ninety_secondths": 1.0/92,
    "ninetythird": 1.0/93, "ninetythirds": 1.0/93, "ninety third": 1.0/93, "ninety thirds": 1.0/93,
    "ninety-third": 1.0/93, "ninety-thirds": 1.0/93, "ninety_third": 1.0/93, "ninety_thirds": 1.0/93,
    "ninetyfourth": 1.0/94, "ninetyfourths": 1.0/94, "ninety fourth": 1.0/94, "ninety fourths": 1.0/94,
    "ninety-fourth": 1.0/94, "ninety-fourths": 1.0/94, "ninety_fourth": 1.0/94, "ninety_fourths": 1.0/94,
    "ninetyfifth": 1.0/95, "ninetyfifths": 1.0/95, "ninety fifth": 1.0/95, "ninety fifths": 1.0/95,
    "ninety-fifth": 1.0/95, "ninety-fifths": 1.0/95, "ninety_fifth": 1.0/95, "ninety_fifths": 1.0/95,
    "ninetysixth": 1.0/96, "ninetysixths": 1.0/96, "ninety sixth": 1.0/96, "ninety sixths": 1.0/96,
    "ninety-sixth": 1.0/96, "ninety-sixths": 1.0/96, "ninety_sixth": 1.0/96, "ninety_sixths": 1.0/96,
    "ninetyseventh": 1.0/97, "ninetysevenths": 1.0/97, "ninety seventh": 1.0/97, "ninety sevenths": 1.0/97,
    "ninety-seventh": 1.0/97, "ninety-sevenths": 1.0/97, "ninety_seventh": 1.0/97, "ninety_sevenths": 1.0/97,
    "ninetyeighth": 1.0/98, "ninetyeighths": 1.0/98, "ninety eighth": 1.0/98, "ninety eighths": 1.0/98,
    "ninety-eighth": 1.0/98, "ninety-eighths": 1.0/98, "ninety_eighth": 1.0/98, "ninety_eighths": 1.0/98,
    "ninetyninth": 1.0/99, "ninetyninths": 1.0/99, "ninety ninth": 1.0/99, "ninety ninths": 1.0/99,
    "ninety-ninth": 1.0/99, "ninety-ninths": 1.0/99, "ninety_ninth": 1.0/99, "ninety_ninths": 1.0/99,
}
cdef dict e_NUM_VALS = <dict>e_NUMBER_VALUES


cpdef bint is_empty(v_val):
    ''' Pass in a value, and check to see if it is empty, nan or blank. '''
    cdef str s_val
    if isinstance(v_val, int):
        return False
    elif isinstance(v_val, (float, np.float32)):
        return np.isnan(v_val)
    elif isinstance(v_val, str):
        s_val = v_val.strip()
        if len(s_val) > 0:
            return False
        else:
            return True
    

cpdef str num_to_string(v_number):
    ''' Check if number should be whole or decimal and return appropriate string. '''
    if isinstance(v_number, str):
        return v_number
    elif isinstance(v_number, int):
        return str(v_number)
    elif isinstance(v_number, (float, np.float32)):
        if np.isnan(v_number):
            return ''
        elif v_number.is_integer():
            return str(int(v_number))
        else:
            return str(v_number)
    else:
        return str(v_number)


cpdef list list_values(list a_values):
    ''' Pass in flat mixed list and get back non-empty values (excluding missing values, nans, empty strings). '''
    cdef a_filled = []
    cdef str s_val
    
    for v_val in a_values:
        if isinstance(v_val, int):
            a_filled.append(v_val)
        elif isinstance(v_val, (float, np.float32)):
            if np.isnan(v_val):
                pass
            else:
                a_filled.append(v_val)
        elif isinstance(v_val, str):
            s_val = v_val.strip()
            if len(s_val) > 0:
                a_filled.append(s_val)
    return a_filled



cpdef double fraction_to_float(v_fraction):
    return to_float(v_fraction)


cpdef double to_float(v_fraction):
    ''' Take a fraction and convert it into a float.
        Can accept formats like "-1/2", "11-1/2", "-11-1/2", "11 1/2", "11 1/2"
    '''
    cdef dict e_fract
    cdef double d_whole, d_num, d_denom, d_dec, d_value
    cdef int i_sign
    
    if isinstance(v_fraction, str):
        o_match = re_FRACTION.search(v_fraction.strip())
        if o_match:
            e_fract = o_match.groupdict()

            if 'num' in e_fract and 'denom' in e_fract:
                if e_fract.get('whole', 0.0):
                    d_value = float(e_fract['whole'])
                else:
                    d_value = 0.0

                d_num = float(e_fract.get('num', 0.0))
                d_denom = float(e_fract.get('denom', 0.0))
                if d_denom > 0:
                    d_value += (d_num / d_denom)

                if d_value > 0:
                    if e_fract.get('sign') == '-':
                        return -d_value
                    else:
                        return d_value
                else:
                    return np.nan
            else:
                return np.nan
            
        else:
            o_match = re_DECIMAL.search(v_fraction.strip())
            if o_match:
                e_fract = o_match.groupdict()
                if 'whole' not in e_fract and 'dec' not in e_fract:
                    return np.nan
                d_value = 0
                
                i_digits = 0
                if e_fract.get('whole', 0.0):
                    d_value += float(e_fract['whole'])
                    i_digits = len(e_fract['whole'])
                    
                if e_fract.get('thousands', 0.0):
                    d_value += float(e_fract['thousands'].replace(",", "")) * (10**i_digits)
                    
                if e_fract.get('dec', 0.0):
                    d_value += float(e_fract['dec'])
                
                if e_fract.get('percent'):
                    d_value /= 100
                
                if e_fract.get('sign') == '-':
                    return -d_value
                else:
                    return d_value
    
    elif isinstance(v_fraction, (float, np.float32)):
        return v_fraction
    elif isinstance(v_fraction, int):
        return float(v_fraction)
    else:
        return np.nan


class NumberSet(set):
    ''' Child class of set that is meant to find the unique/non-equivalent numbers
        allowing for some small variation '''
    def __init__(self, numbers=None, relative_tolerance=0.001):
        ''' Initialize the class.
                If an iterator is provided, load up its numbers into the set
            Arguments:
                numbers: optional {iterator} of str or int or float numbers to initialize set
                relative_tolerance: optional {float} of the relative tolerance allowed in
                    considering numbers to be equivalent
        '''
        self._relative_tolerance = relative_tolerance
        
        if numbers:
            for number in numbers:
                self.add(number)
    
    def add(self, number):
        ''' Add a number to the set if it isn't equivalent to
                an existing value within the specified tolerance
            Arguments: {str}, {int}, or {float}
        '''
        if isinstance(number, (int, float)):
            if self.__contains__(number):
                #print('found exact match:', number)
                pass
            else:
                for d_num in self:
                    if math.isclose(number, d_num, rel_tol = self._relative_tolerance):
                        #print('found loose match:', number)
                        break
                else:
                    #print('add:', number)
                    super().add(number)
        elif isinstance(number, str):
            if re_DIGITS.search(number):
                number = to_float(number)
                
                if self.__contains__(number):
                    #print('found exact match:', number)
                    pass
                else:
                    for d_num in self:
                        if math.isclose(number, d_num, rel_tol = self._relative_tolerance):
                            #print('found loose match:', number)
                            break
                    else:
                        #print('add:', number)
                        super().add(number)
                
            else:
                for re_match, value in get_number_values(number):
                    print('value', value, type(value))
                    if isinstance(value, str):
                        value = to_float(value)
                        
                    if value is None or not isinstance(value, (int, float)): continue
                    print('value', value, type(value))
                    
                    if self.__contains__(value):
                        #print('found exact match:', number)
                        pass
                    else:
                        for d_num in self:
                            if math.isclose(value, d_num, rel_tol = self._relative_tolerance):
                                #print('found loose match:', number)
                                break
                        else:
                            #print('add:', number)
                            super().add(value)


cpdef num_word_value(str piece, default=None):
    #  e_NUMBER_VALUES  e_NUM_VALS
    val = e_NUM_VALS.get(piece)
    if val is not None: return val
    piece = re.sub(r"[^a-z0-9]", piece, flags=re.I)
    val = e_NUM_VALS.get(piece)
    if val is not None: return val
    return default


cpdef get_number_piece_values(str s_number):
    ''' turn a written out text number pattern into a value
    '''
    cdef list a_pieces, a_values
    cdef bint b_valid, b_negative
    cdef dict e_pieces
    cdef float d_decimalfactor
    
    b_valid = False
    re_negative = re.search(r"^(negative[ _]*|positive[ _]*|minus[ _]*|plus[ _]*|less[ _]*|-)", s_number, flags=re.I)
    if re_negative:
        b_negative = True
        s_number = s_number[re_negative.end():]
    else:
        b_negative = False
    
    a_pieces = [
        (
            re_match,
            {piece: value.lower().strip() for piece, value in re_match.groupdict().items() if value}
        )
        for re_match in re_NUMBER_PATTERN.finditer(s_number)
        if re_match.group().strip()
    ]
    #print('a_pieces:', a_pieces)
    
    t_all_word_pieces = tuple([
        value for re_match, e_pieces in a_pieces
        for value in e_pieces.values()])
    #print('t_all_word_pieces', t_all_word_pieces)
    if (
        len(a_pieces) >= 2 and
        all([value in c_DIGIT_WORDS for value in t_all_word_pieces])
    ):
        if t_all_word_pieces in e_NUMBER_WORD_EXCEPTIONS:
            return e_NUMBER_WORD_EXCEPTIONS[t_all_word_pieces]
        else:
            return None
    elif len(a_pieces) == 2:
        if (
            set(a_pieces[0][1].keys()) == {'prefix',} and
            set(a_pieces[1][1].keys()) == {'digit', 'prefix'}
        ):
            if a_pieces[0][1]['prefix'] in c_DIGIT_WORDS:
                a_pieces[0][1]['suffix'] = 'hundred'
        elif (
            set(a_pieces[0][1].keys()) == {'digit', 'prefix'} and
            set(a_pieces[1][1].keys()) == {'digit',}
        ):
            if a_pieces[0][1]['prefix'] in c_TENS_WORDS:
                a_pieces[0][1]['suffix'] = 'ten'
            elif a_pieces[0][1]['prefix'] in c_DIGIT_WORDS:
                a_pieces[0][1]['suffix'] = a_pieces[0][1]['digit']
                a_pieces[0][1]['digit'] = 'hundred'
    
    value, a_values = 0, []
    for re_match, e_pieces in a_pieces:
        if not re_match.group(): continue
        ##print('re_match.group', re_match.group())
        ##a_pieces = re_match.groups()
        ##e_pieces = {piece: value.lower().strip() for piece, value in re_match.groupdict().items() if value}
        #print('e_pieces:', e_pieces)
        
        if len(e_pieces) == 2 and e_pieces.get('prefix') in c_DIGIT_WORDS and e_pieces.get('digit') in c_DIGIT_WORDS:
            print('double digit')
            #return None
            continue
        
        piecevalue = 0
        if e_pieces.get('prefix'):
            piecevalue += num_word_value(e_pieces['prefix'])
            b_valid = True
            
        if e_pieces.get('digit'):
            piecevalue += num_word_value(e_pieces['digit'])
            if e_pieces['digit'] not in {'a', 'no', 'none'}:
                b_valid = True

        if e_pieces.get('suffix'):
            if piecevalue == 0 and str(e_pieces.get('digit')) != 'zero':
                piecevalue = 1
            piecevalue *= num_word_value(e_pieces['suffix'])
            b_valid = True

        if e_pieces.get('numerator') and e_pieces.get('denominator'):
            piecevalue += (
                num_word_value(e_pieces['numerator']) *
                num_word_value(e_pieces['denominator']))
            b_valid = True
        elif e_pieces.get('denominator'):
            if piecevalue:
                piecevalue *= num_word_value(e_pieces['denominator'])
                b_valid = True
            else:
                piecevalue = num_word_value(e_pieces['denominator'])
                b_valid = True
        
        if e_pieces.get('number'):
            d_decimalfactor = to_float(e_pieces['number'])
            piecevalue *= d_decimalfactor
        
        if e_pieces.get('decimal'):
            d_decimalfactor = 0.1
            for s_zero in re.findall("zero", e_pieces['decimal'], flags=re.I):
                d_decimalfactor *= 0.1
            
            piecevalue *= d_decimalfactor
        
        if e_pieces.get('sign'):
            if e_pieces['sign'] in {"-", "minus", "negative", "less"}:
                piecevalue = -piecevalue
                
        #print(piecevalue, re_match.group())
        #value += piecevalue
        a_values.append(piecevalue)
        
    if not b_valid:
        #print('not valid:', a_values, s_number)
        return None
    
    #print('tuple(a_values)', tuple(a_values))
    if tuple(a_values) in e_NUMBER_WORD_EXCEPTIONS:
        return e_NUMBER_WORD_EXCEPTIONS[tuple(a_values)]
    
    value = sum(a_values)
    #print('value:', value, s_number)
    if b_negative:
        value = -value
    return value


cpdef list get_number_values(str s_text, b_constrained=False):
    ''' find the written out numbers in a text and convert them to numbers '''
    cdef list at_numbers
    cdef str s_number, s_sub_number
    
    at_numbers = []
    if not b_constrained:
        for re_match in re_OUTER_NUMBER_PATTERN.finditer(s_text):
            s_number = re_match.group()
            #print('s_number: "{}"'.format(s_number))
            value = get_number_piece_values(s_number)
            
            if value is not None:
                at_numbers.append((re_match, value))
    else:
        for re_match in re_OUTER_NUMBER_PATTERN_CONSTRAINED.finditer(s_text):
            s_number = re_match.group()
            #print('s_number: "{}"'.format(s_number))
            value = get_number_piece_values(s_number)
            
            if value is not None:
                at_numbers.append((re_match, value))
    
    return at_numbers


cpdef int _get_key_regex_sort(tuple element, int pos=0, bint reverse=True, bint use_end=False):
    ''' take a tuple where an element is a regex match object
        and create a sort index for it.
        Arguments:
            element: {tuple} containing a regexmatch object
            pos: {integer specifying the position of the regex match object in the tuple
                default is first position
            reverse: {bool} return regexes from end of string to start
                Default is to sort in reversed order so 
                removing/substituting the regex matches in their source string works cleanly  
            use_end: {bool} use the end position of the regex instead of its start position
    '''
    cdef int idx
    re_match = element[pos]
    if use_end:
        idx = re_match.end()
    else:
        idx = re_match.start()
    
    if reverse:
        return -idx
    else:
        return idx
    
cpdef clean_number_words(str s_text):
    ''' find the written out numbers in a text and convert them to numbers '''
    cdef list at_numbers, at_numbers_constrained
    cdef str s_end
    
    at_numbers = get_number_values(s_text)
    if at_numbers:
        #for re_match, value in sorted(at_numbers, key=lambda t_val: t_val[0].start(), reverse=True):
        for re_match, value in sorted(at_numbers, key=_get_key_regex_sort):
            re_end = re.search(r"[-_ /+,.]*$", re_match.group())
            s_end = re_end.group() if re_end else ''
            if isinstance(value, str):
                s_text = "{}{}{}{}".format(s_text[:re_match.start()], value, s_end, s_text[re_match.end():])
            else:
                s_text = "{}{:g}{}{}".format(s_text[:re_match.start()], value, s_end, s_text[re_match.end():])
    
    at_numbers_constrained = get_number_values(s_text, b_constrained=True)
    if at_numbers_constrained:
        #for re_match, value in sorted(at_numbers, key=lambda t_val: t_val[0].start(), reverse=True):
        for re_match, value in sorted(at_numbers_constrained, key=_get_key_regex_sort):
            re_end = re.search(r"[-_ /+,.]*$", re_match.group())
            s_end = re_end.group() if re_end else ''
            if isinstance(value, str):
                s_text = "{}{}{}{}".format(s_text[:re_match.start()], value, s_end, s_text[re_match.end():])
            else:
                s_text = "{}{:g}{}{}".format(s_text[:re_match.start()], value, s_end, s_text[re_match.end():])
        at_numbers.extend(at_numbers_constrained)
    return s_text, at_numbers


cpdef list list_numbers(list a_values):
    ''' Pass in flat mixed list and get back numbers (integers and non-nan floats)'''
    cdef ad_numbers = []
    cdef str s_val
    
    for v_val in a_values:
        if isinstance(v_val, int):
            ad_numbers.append(v_val)
        elif isinstance(v_val, (float, np.float32)):
            #if isnan(v_val):
            #    pass
            if np.isnan(v_val):
                pass
            else:
                ad_numbers.append(v_val)
        elif isinstance(v_val, str):
            try:
                ad_numbers.append(float(v_val.strip()))
            except:
                if '/' in v_val:
                    s_val = re_NONDIGITS_SIGNS.sub('', v_val)
                    v_val = to_float(s_val)
                    if not np.isnan(v_val):
                        ad_numbers.append(v_val)
                else:
                    s_val = re_NONDIGITS.sub('', v_val)
                    try:
                        ad_numbers.append(float(s_val.strip()))
                    except:
                        pass
    return ad_numbers


cpdef double value_list_sum(list a_values):
    ''' Return sum of non np.nan numbers. '''
    cdef list ad_vals = list_numbers(a_values)
    if len(ad_vals) > 0:
        return sum(ad_vals)
    else:
        return np.nan


cpdef double value_list_avg(list a_values):
    ''' Return average of non np.nan numbers. '''
    cdef list ad_vals = list_numbers(a_values)
    if len(ad_vals) > 0:
        return np.mean(ad_vals)
    else:
        return np.nan

cpdef double value_list_stddev(list a_values):
    ''' Return StdDev of non np.nan numbers. '''
    cdef list ad_vals = list_numbers(a_values)
    try:
        return np.std(ad_vals)
    except:
        return np.nan


cpdef double value_list_herfindahl(list a_values):
    ''' Calculate the HHI from a list of numbers '''
    cdef list ad_vals = list_numbers(a_values)

    cdef float d_total = sum(ad_vals)
    if d_total != 0:
        return sum([(x / d_total)**2 for x in ad_vals])
    return 0


cpdef double value_list_similarity(list a_values):
    ''' Return similarity of non np.nan numbers as factor. '''
    cdef list ad_vals = list_numbers(a_values)
    cdef double d_mean
    if len(ad_vals) > 0:
        d_mean = np.mean(ad_vals)
        if d_mean != 0:
            return 1 - (np.std(ad_vals) / np.mean(ad_vals))
        else:
            # for now return 0.0.
            # The ideal solution would be to adjust the numbers to all possitive then run again
            return 0.0
    else:
        return np.nan


cpdef double value_list_variation(list a_values, bint b_sort=False):
    ''' Return variation of non np.nan numbers as % of largest number. '''
    cdef list ad_vals = list_numbers(a_values)
    if len(ad_vals) > 0:
        d_max, d_min = max(ad_vals), min(ad_vals)
        if d_max == 0: return -d_min
        if d_max < 0: d_max, d_min = -d_min, -d_max
        return ((d_max - d_min) / d_max)
    else:
        return np.nan


cpdef list multi_index(item, list a_list, type vartype=None):
    ''' Find all the indexes of an item in a list
        Arguments:
            item: a variable that might appear in a_list
            a_list: {list} of items that might contain item
        Returns:
            {list} of indexes where item appears in a_list
    '''
    cdef int i, idx, i_num
    cdef str s, s_val
    cdef float d, d_num
    
    if vartype == str:
        if isinstance(item, str):
            s_val = item
        else:
            s_val = str(item)
        return [idx for idx, s in enumerate(a_list) if s == s_val]
    elif vartype == float:
        if isinstance(item, float):
            d_num = item
        else:
            d_num = float(item)
        return [idx for idx, d in enumerate(a_list) if d == d_num]
    elif vartype == int:
        if isinstance(item, int):
            i_num = item
        else:
            i_num = int(item)
        return [idx for idx, i in enumerate(a_list) if i == i_num]
    else:
        return [idx for idx, x in enumerate(a_list) if x == item]


cpdef list group_contiguous_indexes(list a_indexes, b_sort=False):
    ''' join contiguous integer indexes into subgroups in the list '''
    cdef int before, current
    if not a_indexes: return []
    
    if b_sort:
        a_indexes = sorted(a_indexes)
    
    cdef list a_phrases = [[a_indexes[0]]]
    for before, current in zip(a_indexes, a_indexes[1:]):
        if before + 1 == current or before == current:
            a_phrases[-1].append(current)
        else:
            a_phrases.append([current])

    return a_phrases


cpdef merge_dicts(list a_dicts,
                  bint add_numbers=True,
                  bint concat_strings=False, bint merge_strings=False,
                  bint merge_sub_dicts=True, bint merge_lists=True,
                  bint merge_tuples=False, bint reverse=False):
    ''' Merge a list of dictionaries together,
        optionally combining values together depending on type. '''
    cpdef dict e_old, e_new
    if not a_dicts: return {}
    if len(a_dicts) == 1: return a_dicts[0]
    if reverse: a_dicts = list(reversed(a_dicts))
    e_new = {}
    for e_old in a_dicts:
        for v_key, v_item in e_old.items():
            if v_key in e_new:
                v_new_item = e_new[v_key]
                new_type = type(v_new_item)
                old_type = type(v_item)
                
                if new_type != old_type:
                    pass
                elif new_type == dict and merge_sub_dicts:
                    e_new[v_key] = merge_dicts(
                        [v_new_item, v_item],
                        add_numbers=add_numbers,
                        concat_strings=concat_strings,
                        merge_strings=merge_strings,
                        merge_sub_dicts=merge_sub_dicts,
                        merge_lists=merge_lists,
                        merge_tuples=merge_tuples) #, reverse)
                elif new_type == list and merge_lists:
                    e_new[v_key].extend((v_item))
                elif new_type == str and concat_strings:
                    e_new[v_key] += v_item
                elif new_type == str and merge_strings:
                    if v_key not in e_new:
                        e_new[v_key] = v_item
                    elif isinstance(e_new.get(v_key), list):
                        e_new[v_key].append(v_item)
                    elif isinstance(e_new.get(v_key), str):
                        e_new[v_key] = [e_new[v_key], v_item]
                elif new_type == int and add_numbers:
                    e_new[v_key] += v_item
                elif new_type == float and add_numbers:
                    e_new[v_key] += v_item
                elif new_type == tuple and merge_tuples:
                    e_new[v_key] += v_item
                else:
                    pass
            else:
                if isinstance(v_item, (dict, list)):
                    e_new[v_key] = v_item.copy()
                else:
                    e_new[v_key] = v_item
    return e_new


cpdef int count_children(variable, bint include_self=True):
    ''' Recursively count the number of child variables under the passed in variable,
        whether they are in lists, sets, tuples, dicts.
        Arguments:
            include_self includes the current variable even if it is empty (but not None)
        Returns:
            {int} count of child variables
    '''
    cdef int i_count
    if variable is None:
        return 0
    elif not variable:
        if include_self:
            return 1
        else:
            return 0
    elif isinstance(variable, (str, int, float)):
        return 1
    elif isinstance(variable, (list, tuple, set)):
        i_count = 1 if include_self else 0
        i_count += sum([count_children(sub, include_self) for sub in variable])
        return i_count
    elif isinstance(variable, dict):
        i_count = 1 if include_self else 0
        i_count += sum([count_children(sub, include_self) for sub in variable.values()])
        return i_count
    else:
        return 1


cpdef int child_depth(variable, int depth=0):
    ''' Recursively determine the max container/variable depth in nested variables
        whether they are in lists, sets, tuples, dicts.
        Arguments:
            depth: {int} the current depth
        Returns:
            {int} the max depth of this levels children
    '''
    cdef list a_sub_depths
    if variable is None:
        pass
    elif not variable:
        pass
    elif isinstance(variable, (str, int, float)):
        pass
    elif isinstance(variable, (list, tuple, set)):
        a_sub_depths = [
            child_depth(sub, depth=depth + 1)
            for sub in variable]
        depth = max([depth] + a_sub_depths)
    elif isinstance(variable, dict):
        a_sub_depths = [
            child_depth(sub, depth=depth + 1)
            for sub in variable.values()]
        depth = max([depth] + a_sub_depths)
    return depth



cpdef str add_suffix_keep_length(str s_text, int i_suffix, str s_sep='_', int i_max_len=0):
    ''' Add an integer suffix to a string while keeping the whole string under a maximum length
        Arguments:
            s_text: {str} the text value
            i_suffix: {int} the number to append
            s_sep: {str} char(s) to insert between text and integer suffix
            i_max_len: {int} the maximum total length of the string. Must be longer than suffix alone
        Returns:
            {str} of text + suffix
    '''
    cdef str s_suffix = '{}{}'.format(s_sep, i_suffix)
    if i_max_len > 0 and i_max_len < len(s_suffix) + 1:
        raise Exception('Max length too short to add suffix')
    if i_max_len > 0:
        return s_text[:i_max_len - len(s_suffix)] + s_suffix
    else:
        return s_text + s_suffix


cpdef list make_strings_unique(list a_strings, int i_max_len=0, f_cleaning_function=None):
    ''' Take a list of strings and make sure they are all unique by appending numbers as necessary
        Arguments:
            a_strings: {list} of strings
            i_max_len: {int} the maximum total length of any string. Must be longer than suffix alone
            f_cleaning_function: optional {function} for cleaning strings before testing uniqueness
    '''
    cdef set c_used = set()
    cdef list a_unique = []
    cdef int idx
    cdef str s_clean, s_used
    
    for txt in a_strings:
        if not isinstance(txt, str):
            a_unique.append(txt)
            continue
        
        s_clean = txt
        if f_cleaning_function is not None:
            s_clean = f_cleaning_function(s_clean)
        if i_max_len > 0:
            s_clean = s_clean[:i_max_len]
        
        if s_clean in c_used:
            idx = 1
            s_used = add_suffix_keep_length(s_clean, idx, i_max_len=i_max_len)
            while s_used in c_used:
                idx += 1
                s_used = add_suffix_keep_length(s_clean, idx, i_max_len=i_max_len)
            s_clean = s_used
        a_unique.append(s_clean)
        c_used.add(s_clean)
    return a_unique


cpdef float vector_similarity(vec1, vec2):
    ''' Calculate cosine similarity/distance between two large-dimensional arrays. '''
    if vec1 is None or vec2 is None: return 0.0
    return np.dot(vec1, vec2) / (np.linalg.norm(vec1) * np.linalg.norm(vec2))
    
    
def average_vectors(*args, int i_dimensions=0, list a_weights=None):
    ''' Calculate the average centerpoint of a group of vectors.
        Optionally use weights. '''
    cdef list a_vectors
    cdef int i_valid_vectors
    cdef float d_total_weights, d_weight_adjustment
    
    if isinstance(args[0], list): a_vectors = args[0]
    elif isinstance(args[0], tuple): a_vectors = list(args[0])
    else: a_vectors = list(args)
    if i_dimensions == 0:
        for vec in a_vectors:
            if vec is None: continue
            i_dimensions = len(vec)
            break
        if i_dimensions == 0: return None
    
    new_vec = np.zeros((i_dimensions,), dtype="float32")
    i_valid_vectors = 0
    d_total_weights = 0
    if not a_weights:
        for vec in a_vectors:
            if vec is None: continue
            i_valid_vectors += 1
            new_vec = np.add(new_vec, vec)
        if i_valid_vectors == 0: return None
    else:
        for vec, weight in zip(a_vectors, a_weights):
            if vec is None or weight is None: continue
            i_valid_vectors += 1
            d_total_weights += weight
        
        if i_valid_vectors == 0 or d_total_weights == 0: return None
        d_weight_adjustment = d_total_weights / i_valid_vectors
        for vec, weight in zip(a_vectors, a_weights):
            if vec is None or weight is None: continue
            new_vec = np.add(new_vec, np.multiply(vec, weight / d_weight_adjustment))
    return np.divide(new_vec, i_valid_vectors)
    

def filter_vectors(a_vectors, a_labels=None):
    ''' Pass in list of vector arrays and string labels.
        Return filtered lists with no None or NaN values. '''
    a_new_vectors = []
    if a_labels:
        a_new_labels = []
        for vector, label in zip(a_vectors, a_labels):
            if vector is not None and not np.isnan(np.sum(vector)):
                a_new_vectors.append(vector)
                a_new_labels.append(label)
        return a_new_vectors, a_new_labels
    else:
        for vector in a_vectors:
            if vector is not None and not np.isnan(np.sum(vector)):
                a_new_vectors.append(vector)
        return a_new_vectors


def plottable_vectors(a_vectors, a_labels=None, dimension_transformer=None, i_dimensions=3):
    ''' Reduce the dimensions using a passed in fitted transformer with a transform function,
        or by a creating a PCA transformer and using that. '''
    np_vector_table = np.vstack([vec for vec in a_vectors if vec is not None])
    # the array of vectors is the X element
    # the labels are the Y element

    if i_dimensions >= np_vector_table.shape[0]:
        i_dimensions = np_vector_table.shape[0] - 1
    
    if np_vector_table.shape[0] == i_dimensions:
        np_reduced_vectors = np_vector_table
    else:
        if dimension_transformer and dimension_transformer not in ['PCA', 'LDA', 'pca', 'lda']:
            # print("Using passed in transformer")
            o_transformer = dimension_transformer
            np_reduced_vectors = o_transformer.transform(np_vector_table)
        elif dimension_transformer in ['LDA', 'lda']:
            # print("Using LDA transformer")
            o_transformer = LDA(n_components=i_dimensions)
            np_reduced_vectors = o_transformer.fit_transform(np_vector_table, a_labels)
            dimension_transformer = o_transformer
        else:
            # print("Using PCA transformer")
            o_transformer = PCA(i_dimensions)
            np_reduced_vectors = o_transformer.fit_transform(np_vector_table)
            dimension_transformer = o_transformer
    dimension_transformer = o_transformer
    
    return np_reduced_vectors.transpose()
    

# this is deliberately non-pythonic to focus completely on speed.
cpdef list delimited_strings_to_dicts(list a_dict_strings,
                                      str s_pairs_delimiter=",",
                                      str s_key_val_delimiter="="):
    ''' Take in a list of strings containing delimited key/values.
        Return list of dictionaries. '''
    cdef str s_row, s_key, s_val
    cdef int i_char, i_key_start, i_val_start, i_len
    cdef dict e_row
    cdef list ae_rows = []
    
    i_char = 0
    for s_row in a_dict_strings:
        i_len = len(s_row)
        if i_len == 0:
            ae_rows.append({})
            continue
            
        e_row, s_key, s_val, i_key_start, i_val_start, i_old_key = ({}, '', '', 0, 0, 0)
        for i_char in range(i_len):
            if s_row[i_char] == s_pairs_delimiter:
                if i_char <= i_key_start:  # handle repeated pair delimiters
                    i_key_start = i_char + 1
                elif i_val_start > i_key_start:
                    s_key = s_row[i_key_start:i_val_start - 1].strip()
                    s_val = s_row[i_val_start:i_char].strip()
                    
                    e_row[s_key] = s_val
                    i_old_key = i_key_start
                    i_key_start = i_char + 1
                else:
                    ''' There wasn't a key-val delimiter since this is the last key
                        so make the last key include this value also.
                        This handles things like extra commas inside the value
                        when commas are also the attribute delimiter '''
                    i_key_start = i_old_key
                    
            elif s_row[i_char] == s_key_val_delimiter:
                i_val_start = i_char + 1
        
        # finish up string
        if i_key_start < i_char:
            s_key = s_row[i_key_start:i_val_start - 1].strip()
            s_val = s_row[i_val_start:i_len].strip()
            e_row[s_key] = s_val
                    
        ae_rows.append(e_row)
        i_char = 0
    
    return ae_rows


cpdef  float mean_of_products(list a_values1, list a_values2):
    """ The mean of the products of the corresponding values of bivariate data
        Arguments:
            a_values1: {list} of numbers
            a_values2: {list} of numbers
    """
    cdef float total
    cdef int i_len = min(len(a_values1), len(a_values2))
    
    if i_len == 0: return 0.0
    total = 0.0
    
    for x, y in zip(a_values1, a_values2):
        total += (x * y)
    return total / i_len


cpdef float pearson_correlation(list a_independent, list a_dependent):
    """ Implements Pearson's Correlation, using several utility functions to
        calculate intermediate values before calculating and returning rho.
        Arguments:
            a_independent: {list} of numbers to check for correlation against a_dependent
            a_dependent: {list} of numbers to check for correlation against a_independent
        Returns:
            {float}
    """
    cdef float independent_mean, dependent_mean, products_mean, covariance
    cdef float independent_standard_deviation, dependent_standard_deviation, stdevs, rho
    
    if not a_independent or not a_dependent: return 0.0
    
    if len(a_independent) != len(a_dependent):
        #if len(a_independent) < len(a_dependent):
        a_dependent = a_dependent[:len(a_independent)]
        a_independent = a_independent[:len(a_dependent)]
    
    # covariance
    independent_mean = np.mean(a_independent)
    dependent_mean = np.mean(a_dependent)
    products_mean = mean_of_products(a_independent, a_dependent)
    covariance = products_mean - (independent_mean * dependent_mean)

    # standard deviations of independent values
    independent_standard_deviation = value_list_stddev(a_independent)

    # standard deviations of dependent values
    dependent_standard_deviation = value_list_stddev(a_dependent)

    # Pearson Correlation Coefficient
    stdevs = independent_standard_deviation * dependent_standard_deviation
    rho = covariance / stdevs if stdevs != 0 else 0.0
    return rho


cpdef list moving_average(list a_values, float d_smoothing_factor=0.5):
    ''' A form of a moving average that adjusts each subsequent
        number based on the one before it and a smoothing factor.
        This particular implementation has very little lag.
        Arguments:
            a_values: {list} of numbers
            d_smoothing_factor: {float} between 0-1 of how much use of the
                previous number vs the current number
                Smaller number means being closer to the original list
        Returns:
            {list} of smoothed numbers
    '''
    cdef float d_inverse, d_moving, d_val, d_new
    cdef list a_new
    
    if len(a_values) < 2 or d_smoothing_factor <= 0: return a_values
    
    d_inverse = 1 - d_smoothing_factor
    d_moving = a_values[0]
    a_new = []
    
    for d_val in a_values:
        d_new = (d_val * d_inverse) + (d_moving * d_smoothing_factor)
        a_new.append(d_new)
        d_moving = d_new
    return a_new


cpdef list smooth(list a_values, float d_smoothing_factor=0.5):
    ''' Smooth a list of numbers using bi-directional moving averages
        going forward and backward
        Arguments:
            a_values: {list} of numbers
            d_smoothing_factor: {float} between 0-1 of how much use of the
                previous number vs the current number
                Smaller number means being closer to the original list
        Returns:
            {list} of smoothed numbers
    '''
    cpdef list a_smooth
    cdef float x, y
    a_smooth = [
        round((x + y) / 2, 2)
        for x, y in zip(
            moving_average(a_values, d_smoothing_factor),
            reversed(moving_average(list(reversed(a_values)), d_smoothing_factor)))
    ]
    return a_smooth
